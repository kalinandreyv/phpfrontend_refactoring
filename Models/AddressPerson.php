<?php

namespace App;

class AddressPerson extends Model
{

    function getSimpelListOfEmployees($addressId)
    {
        $sql = "SELECT p.id, p.name, IFNULL(ap.role, p.role) as role 
                FROM rl_address_people as ap
                JOIN rl_people as p ON p.id = ap.person_id
                WHERE ap.address_id = $addressId ";

        $employees = $this->DBWithConnection()->select($sql);

        return response()->json($employees);
    }

    function isPersonAnEmployee($address, $personId)
    {
        $sql = "SELECT * 
                FROM rl_address_people 
                WHERE address_id = $address->id AND person_id = $personId";

        $result = $this->DBWithConnection()->select($sql);

        return !empty($result);
    }

    function findLabWorkers($related_labs_ids = [])
    {
        $lab_workers = [];

        if($related_labs_ids)
        {
            $sql = "SELECT rl.id, rl.name, ap.address_id, pt.name as 'workerType' 
                    FROM rl_people rl  
                    JOIN rl_address_people ap ON ap.person_id = rl.id 
                    JOIN rl_people_types pt ON rl.type_id = pt.id  
                    WHERE ap.address_id IN (" . $related_labs_ids . ")";

            $lab_workers = $this->DBWithConnection()->select(DB::raw($sql));
        }

        return $lab_workers;
    }

    function findRelatedLabWorkers($related_labs_ids = [])
    {
        $related_people = [];

        if ($related_labs_ids != "") {
            $sql = "SELECT p.id, p.name, ap.address_id 
                    FROM rl_address_people ap 
                    JOIN rl_people p ON ap.person_id = p.id  
                    WHERE ap.address_id IN (" . $related_labs_ids . ")";

            $related_people = $this->DBWithConnection()->select(DB::raw($sql));
        }

        return $related_people;
    }

    function getWorkersRelationships($related_people_ids = [])
    {
        $people_relationships = [];

        if($related_people_ids) {
            $sql = "SELECT ac.from_person_id, ac.to_person_id, ac.edge_weight, act.id as 'connection_type' 
                    FROM rl_address_connections ac 
                    LEFT JOIN rl_address_connection_types act on ac.edge_type = act.id 
                    WHERE ac.from_person_id IN (" . $related_people_ids . ") AND ac.to_person_id IN (" . $related_people_ids . ") ";

            $people_relationships = $this->DBWithConnection()->select(DB::raw($sql));
        }

        return $people_relationships;
    }

    function getEmployeeRoleList($id)
    {
        $sql = "SELECT DISTINCT(IF(role <> '', role, '--empty--')) as role FROM rl_address_people WHERE address_id = ? ORDER BY role ASC";

        $result = $this->DBWithConnection()->select($sql, [$id]);

        return array_pluck($result, 'role');
    }

}
