<?php

namespace App;

class Cluster extends Model
{

    public function updateClusters($cluster_id = null)
    {
        $cluster = $this->with('addresses')
            ->whereId($cluster_id)
            ->first();
        if ($cluster && $cluster->addresses->count() < 1) {
            $cluster->delete();
        }
    }

    function getClusterLabs($address_id = null)
    {
        $cluster_labs = [];

        if($address_id)
        {
            $sqlQuery = "SELECT a2.id, a2.name, a2.cluster_id FROM rl_addresses a JOIN rl_addresses a2 WHERE (a.cluster_id = a2.cluster_id OR a2.id = ? ) AND a.id = ?";
            $cluster_labs = array_pluck($this->DBWithConnection()->select(DB::raw($sqlQuery), [$address_id, $address_id]), 'id');
        }

        return $cluster_labs;
    }

    function getClusterStaff($address = [], $params = [])
    {
        $people = People::with('addresses')
            ->whereHas('addresses', function ($q) use ($address) {
                $q->where('cluster_id', $address->cluster_id);
            });

        if (isset($params['name'])) {
            $people->where('rl_people.name', 'like', '%'.$params['name'].'%');
        }

        if (isset($params['type'])) {
            $people->where('rl_people.type_id', $params['type']);
        }

        return $people->orderByRaw('name')->paginate(10);
    }

    function getClusterProducts($address = [])
    {
        $products = Product::with([
            'addresses' => function ($query) use ($address) {
                $query->where('cluster_id', $address->cluster_id);
            }])->whereHas('addresses', function ($q) use ($address) {
            $q->where('cluster_id', $address->cluster_id);
        })->orderByRaw('company, name')->paginate(10);

        return $products;
    }

}
