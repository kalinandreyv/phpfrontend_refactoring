<?php

namespace App;

class Tag extends Model
{

    function getTags()
    {
        return $this->getResultFromCache("
                                SELECT t1.*
                        FROM rl_tags as t1
                        WHERE t1.parent_tag_id IS NULL OR t1.parent_tag_id = 0
                        GROUP BY t1.id
                        
                        UNION
                        
                        SELECT t2.*
                        FROM rl_tags as t2
                        JOIN rl_tags as t3
                          ON t3.parent_tag_id = t2.id
                        WHERE t2.parent_tag_id IS NULL OR t2.parent_tag_id = 0
                        GROUP BY t2.id
                        
                        ORDER BY name
                    ");
    }

    function getTagsRelational($tags = [])
    {
        $relationalTags = [];

        if($tags) {

            $relationalTags = collect($tags);

            $relationalTags->each(function ($tag) {
                $tag->childProducts = Tag::where('parent_tag_id', $tag->id)
                    ->orderBy('name')
                    ->groupBy('rl_tags.id')
                    ->get(['id', 'name']);
            });
        }

        return $relationalTags;
    }

}
