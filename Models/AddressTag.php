<?php

namespace App;

class AddressTag extends Model
{

    function addTagsToAddresses($addresses)
    {
        foreach ($addresses as $i => $address) {
            $sql = "SELECT GROUP_CONCAT(t2.name SEPARATOR ', ') as tags_str
                            FROM `rl_tags` as t2
                            LEFT JOIN rl_address_tags AS at2
                            ON at2.tag_id = t2.id
                            WHERE at2.address_id = $address->id
                            GROUP BY at2.address_id
                      ";

            $tag = $this->DBWithConnection()->select($sql);

            $tagsStr = !empty($tag) ? $tag[0]->tags_str : '';

            $addresses[$i]->tags_str = $tagsStr;
        };
    }

}
