<?php

namespace App;

class ExpandersEvents extends Model
{

    function getMaxScore($address_id = null)
    {
        $sql = 'SELECT ROUND(MAX(score), 2) as max_score FROM us_expanders_events WHERE category_id = 4';
        $address_id ? $sql .= " AND address_id = $address_id" : null;

        return $this->getResultFromCache($sql)[0]->max_score;
    }

}
