<?php

namespace App;

class SwitzerlandCanton extends Model
{

    function getCantonsRelational()
    {
        $relationalCantons = SwitzerlandCanton::where('parent_id', 0)->get();
        $relationalCantons->each(function ($canton) {
            $canton->childProducts = SwitzerlandCanton::where('parent_id', $canton->id)
                ->orderBy('name')
                ->groupBy('rl_geo_hierarchy.id')
                ->get(['id', 'name']);
        });
    }


}
