<?php

namespace App;

class Product extends Model
{

    function getProductsRelational()
    {
        $relationalProducts = Product::where(function ($q){
            $q->whereRaw("rl_products.name IS NULL OR rl_products.name = ''");
        })
            ->orderByRaw('company, name')
            ->get();

        $relationalProducts->each(function ($product) {
            $product->childProducts = Product::where('company', $product->company)
                ->orderByRaw('company, name')
                ->get();
        });

        return $relationalProducts;
    }

}
