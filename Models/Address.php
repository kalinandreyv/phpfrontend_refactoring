<?php

namespace App;

class Address extends Model
{

    function updateAddressDetails($address = [], $params = [])
    {
        $address->update($params);

        $tags = request()->get('tags');

        $ids = [];

        foreach ($tags as $tag) {

            $tagName = isset($tag['name']) ? $tag['name'] : $tag;

            if ( ! Tag::whereName($tagName)->first()) {
                $newTag = new Tag();
                $newTag->name = $tagName;
                $newTag->save();
                $ids[] = $newTag->id;
            } else {
                $ids[] = $tag['id'];
            }
        }

        UserEdit::logManyToMany($address, 'rl_tags', $ids, $address->tags()->pluck('id')->toArray());

        $address->tags()->sync($ids);

        return $address;
    }

    function prepareAddressesQuery()
    {
        $query = $this->with('tags')
            ->with('cluster')
            ->withCount('people')
            ->with(['products' => function($q){
                $q->select('id');
                $q->orderByRaw('company, name');
            }]);

        $query = $this->composeConditions($query, request()->all());

        return $query;
    }

    function addGrowthOpp($addresses)
    {
        foreach ($addresses as $i => $address) {
            $sql = "SELECT sv1.scalar_value as growthopp 
                    FROM rl_addresses as a
                    LEFT JOIN rsm_scalar_values AS sv1 
                      ON sv1.rsm_id = a.RSMf_SiteMasterId 
                      AND sv1.franchise = 'Enterprise' 
                      AND sv1.scalar_type_id = 10
                    WHERE a.id = $address->id
                    ";

            $res = $this->DBWithConnection()->select($sql);

            $growthOpp = !empty($res) ? $res[0]->growthopp : '';

            $addresses[$i]->growthopp = intval($growthOpp);
        }
    }

    function showSimpleDetails($id)
    {
        $address = $this->find($id);

        return response()->json($address);
    }

    function countAddressesForUSA() {

        $sql = "SELECT COUNT(DISTINCT a.id) as cnt FROM rl_addresses AS a";

        $sql .= $this->defineJoinsForUSA();

        $sql .= " WHERE a.lat <> 0 AND a.lon <> 0";

        $sql .= $this->composeConditionsUsa();

        $result = $this->DBWithConnection()->select(DB::raw($sql));

        return $result[0]->cnt;
    }

    function findAddressesAndGroupByState()
    {

        $subSubSql = $this->checkIfRequestedMin2CompaniesInCluster();

        $subSql = "SELECT rms.physical_state as state_abv, COUNT(DISTINCT $subSubSql) as total_addresses 
                FROM rl_addresses AS a";

        $subSql .= $this->defineJoinsForUSA();

        $subSql .= " WHERE a.lat <> 0 AND a.lon <> 0";

        $subSql .= $this->composeConditionsUsa();

        $subSql .= " GROUP BY rms.physical_state";

        $sql = "SELECT st.name, st.lon, st.lat, state_abv, total_addresses
            FROM ($subSql) subsql
            JOIN rl_usa_state_coords as st ON st.abv = state_abv
        ";

        return $result = $this->getResultFromCache($sql);
    }

    function findAddressesAndGroupByLatLonTile()
    {
        $sql = "SELECT a.latlon_tile, a.name, COUNT(a.id) as total_addresses 
                FROM rl_addresses as a
                WHERE a.latlon_tile IS NOT NULL
                AND a.latlon_tile <> '0|0'
                GROUP BY a.latlon_tile";

        $result = $this->DBWithConnection()->select($sql);

        $data = [];

        foreach ($result as $i => $row) {

            $latLonArray = explode('|', $row->latlon_tile);

            $data[$i] = [];

            $data[$i]['name'] = $row->total_addresses > 1 ? $row->total_addresses : $row->name;
            $data[$i]['lat'] = $lat = intval($latLonArray[0]) * 0.05;
            $data[$i]['lon'] = $lon = intval($latLonArray[1]) * 0.106;
            $data[$i]['state_abv'] = $lat.';'.$lon;
            $data[$i]['total_addresses'] = $row->total_addresses;
            $data[$i]['total_addresses_abbrev'] = (string)$this->getAbbreviationValue($row->total_addresses);
        }

        return $data;
    }

    function prepareSqlForFetchAddressesWithoutGroupingByState($isForPagination = false)
    {
        $sql = "SELECT a.id, a.name, a.lat, a.lon, a.customer_status, acomp.people_count";

        if(env('APP_SCOPE') === 'wealthscape') {
            $sql .= ', a.assigned_to';
        }

        if ($isForPagination) {
            $sql .= ", a.address, cl.name as cluster_name";
        }

        if(Auth::user()->default_country === 'usa-private' && request()->get('sort-by') === 'growthopp-desc') {
            $sql .= ", sv1.scalar_value as growthopp";
        }

        if(Auth::user()->default_country === 'usa-private' && request()->get('sort-by') === 'actualsales-desc') {
            $sql .= ", sv2.scalar_value as actualsales";
        }

        $sql .= " FROM rl_addresses AS a";

        $sql .= $this->defineJoinsForUSA($isForPagination);

        $sql .= " WHERE a.lat <> 0 
                    AND a.lon <> 0 
                    AND a.lat IS NOT NULL 
                    AND a.lon IS NOT NULL";

        $sql .= $this->composeConditionsUsa();

        $sql .= ' GROUP BY a.id ';

        return $sql;
    }

    private function defineJoinsForUSA($isForPagination = false)
    {
        $requestParams = request()->all();

        $mainSql = ' LEFT JOIN rl_address_precomputed_props as acomp ON acomp.address_id = a.id';

        if(Auth::user()->default_country == 'usa-private') {
            $mainSql .= ' LEFT JOIN rl_addresses_us_rms AS rms ON a.RSMf_SiteMasterId = rms.RSMf_SiteMasterId';

            if(isset($requestParams['scalar-types'])) {
                $mainSql .= GlobalHelper::addScalarValueJoins($requestParams);
            }

            if (isset($requestParams['zones'])
                || isset($requestParams['areas'])
                || isset($requestParams['franchises'])
                || isset($requestParams['tag-ids'])
            ) {
                $mainSql .= ' LEFT JOIN rl_addresses_us_rms_ext AS are ON a.RSMf_SiteMasterId = are.RSMf_SiteMasterId';
            }

            if(isset($requestParams['sort-by']) && $requestParams['sort-by'] === 'growthopp-desc') {
                $mainSql .= " LEFT JOIN rsm_scalar_values AS sv1 ON sv1.rsm_id = a.RSMf_SiteMasterId AND sv1.franchise = 'Enterprise' AND sv1.scalar_type_id = 10 ";
            }

            if(isset($requestParams['sort-by']) && $requestParams['sort-by'] === 'actualsales-desc') {
                $mainSql .= " LEFT JOIN rsm_scalar_values AS sv2 ON sv2.rsm_id = a.RSMf_SiteMasterId AND sv2.franchise = 'Enterprise' AND sv2.scalar_type_id = 11 ";
            }
        }

        if (Auth::user()->default_country !== 'usa-private' && array_key_exists('scalar-types', $requestParams)) {
            $mainSql .= ' LEFT JOIN rl_scalar_values AS sv ON a.id = sv.address_id';
        }

        if ($isForPagination) {
            $mainSql .= ' LEFT JOIN rl_clusters AS cl ON a.cluster_id = cl.id';
        }

        if(isset($requestParams['tag-ids'])) {
            $mainSql .= " LEFT JOIN rl_address_tags AS at
                            ON at.address_id = a.id
                         LEFT JOIN rl_tags AS t
                            ON t.id = at.tag_id";
        }

        if(isset($requestParams['used-product-ids'])) {
            $mainSql .= " LEFT JOIN rl_address_products AS aprod
                            ON a.id = aprod.address_id
                         LEFT JOIN rl_products AS prod
                            ON prod.id = aprod.product_id";
        }

        return $mainSql;
    }

    private function composeConditions($query, $requestParams)
    {

        if (isset($requestParams['sort-by'])) {

            $field = explode('-',$requestParams['sort-by'])[0];
            $direction = explode('-',$requestParams['sort-by'])[1];

            if($field == 'people') {
                $field .= '_count';
            }
            else if($field == 'products') {
                $query->withCount('products');
                $field .= '_count';
            }

            $query->orderBy($field,$direction);
        }

        if (isset($requestParams['tag-ids'])) {
            $query->whereHas('tags', function ($q) use ($requestParams) {
                $q->whereIn('id', $requestParams['tag-ids']);
            });
        }

        if (isset($requestParams['used-product-ids'])) {
            $query->whereHas('products', function ($q) use ($requestParams) {
                $q->whereIn('id', $requestParams['used-product-ids']);
            });
        }

        if (isset($requestParams['type-id'])) {
            $query->whereHas('customerType', function ($q) use ($requestParams) {
                $q->where('id', $requestParams['type-id']);
            });
        }

        if (isset($requestParams['global-search'])) {
            $query->where('rl_addresses.name', 'LIKE', '%'.$requestParams['global-search'].'%');
        }

        if (isset($requestParams['name'])) {
            $query->where('rl_addresses.name', 'LIKE', '%'.$requestParams['name'].'%');
        }

        if (isset($requestParams['iteration'])) {

            $addressIds = $this->getAddressIdsForIteration($requestParams);

            $query->whereIn('rl_addresses.id', $addressIds);

            $query->orderByRaw('FIELD(rl_addresses.id, '. implode(', ', $addressIds).')');
        }

        if (isset($requestParams['address-ids'])) {
            $query->whereIn('rl_addresses.id', explode(',',$requestParams['address-ids']));
        }

        if (isset($requestParams['cluster-id'])) {
            $query->where('cluster_id', $requestParams['cluster-id']);
        }

        if (isset($requestParams['at-least-2-companies'])) {
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();

            $query->whereIn('rl_addresses.cluster_id', $clusterIds);
        }

        return $query;
    }

    private function composeConditionsUsa()
    {
        $sql = '';

        $requestParams = request()->validate([
            'tag-ids' => 'array|nullable',
            'used-product-ids' => 'array|nullable',
            'type-id' => 'integer|nullable',
            'name' => 'string|nullable',
            'global-search' => 'string|nullable',
            'tl_lat' => 'numeric|nullable',
            'tl_lon' => 'numeric|nullable',
            'br_lat' => 'numeric|nullable',
            'br_lon' => 'numeric|nullable',
            'state' => 'string',
            'cluster-id' => 'integer|nullable',
            'assignee.*' => 'integer',
            'states.*' => 'integer',
            'zones' => 'array|nullable',
            'areas' => 'array|nullable',
            'baden-cities' => 'array|nullable',
            'is-favorite' => 'sometimes|required|numeric',
            'iteration' => 'sometimes|required|array',
            'scalar-types' => 'sometimes|required|array',
            'market_programs' => 'sometimes|required|array',
        ]);

        if (isset($requestParams['tag-ids']) && Auth::user()->default_country !== 'usa-private') {
            $sql .= ' AND t.id IN ('.implode(',', $requestParams['tag-ids']).')';
        }
        elseif (isset($requestParams['tag-ids']) && Auth::user()->default_country === 'usa-private') {
            $sql .= LabscapeRocheService::getAddressTagSqlCondition($requestParams['tag-ids']);
        }


        if (isset($requestParams['used-product-ids']) && Auth::user()->default_country !== 'usa-private') {
            $sql .= ' AND prod.id IN ('.implode(',', $requestParams['used-product-ids']).')';
        }

        if (isset($requestParams['used-product-ids']) && Auth::user()->default_country === 'usa-private') {
            $ids = LabscapeRocheService::selectCollectionOfAddressIdsByProducts($requestParams['used-product-ids'], $this);

            $idsStr = implode(',', $ids);

            $sql .= ' AND a.id IN ('.$idsStr.') ';
        }

        if (isset($requestParams['type-id'])) {

            if($requestParams['type-id'] != -1) {
                $sql .= ' AND a.customer_status = '.$requestParams['type-id'];
            }
            else {
                $ignoredAddressIds = AddressIgnore::getUserIgnoredAddressesIds();

                $ignoredAddressIdsStr = !empty($ignoredAddressIds) ? implode(',', $ignoredAddressIds) : -1;

                $sql .= " AND a.id IN ($ignoredAddressIdsStr)";
            }
        }

        if (isset($requestParams['global-search'])) {
            $sql .= ' AND a.name LIKE %'.$requestParams['global-search'].'%';
        }

        if (isset($requestParams['name'])) {
            $sql .= ' AND a.name LIKE %'.$requestParams['name'].'%';
        }

        if (isset($requestParams['iteration'])) {

            $addressIds = $this->getAddressIdsForIteration($requestParams);

            $this->addressIdsDefinedWhenIteration = $addressIds;

            $sql .= ' AND a.id IN('.implode(',', $addressIds).')';
        }

        if (isset($requestParams['tl_lat']) && isset($requestParams['tl_lon'])) {

            $p = $requestParams;

            $sql .= " AND ( a.lat < $p[tl_lat] AND a.lat > $p[br_lat] AND a.lon > $p[tl_lon] AND a.lon < $p[br_lon] )";
        }

        if (isset($requestParams['state'])) {
            $p = $requestParams;

            $sql .= " AND  rms.physical_state = '$p[state]'";
        }

        if (isset($requestParams['cluster-id'])) {
            $sql .= ' AND a.cluster_id = '.$requestParams['cluster-id'];
        }

        if (isset($requestParams['at-least-2-companies'])) {
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();

            $sql .= ' AND a.cluster_id IN ('.implode(',',$clusterIds).') ';
        }

        if (isset($requestParams['is-favorite'])) {

            $favoriteIds = Favorite::getFavoriteEntityIds('ADDRESS');

            if(empty($favoriteIds)) {
                $sql .= ' AND a.id IN (-1) ';
            }
            else {
                $sql .= ' AND a.id IN ('.implode(',',$favoriteIds).') ';
            }

        }

        if (isset($requestParams['assignee'])) {
            $sql .= ' AND a.assigned_to IN ('.implode(',', $requestParams['assignee']).')';
        }

        if (isset($requestParams['market_programs'])) {

            $ids = LabscapeRocheService::selectCollectionOfIdsByMarketPrograms($requestParams['market_programs'], $this);

            $idsStr = implode(',', $ids);

            $sql .= ' AND a.id IN ('.$idsStr.')';
        }

        if (isset($requestParams['states'])) {

            $statesSql = "SELECT abv FROM rl_usa_state_coords WHERE id IN (".implode(',', $requestParams['states']).")";

            $statesAbbrev = array_pluck($this->DBWithConnection()->select($statesSql), 'abv');

            $statesAbbrev = array_map(function($el){return "'$el'";}, $statesAbbrev);

            $sql .= ' AND rms.physical_state IN ('.implode(',', $statesAbbrev).')';
        }

        if (isset($requestParams['baden-cities']) && env('APP_SCOPE') === 'wealthscape') {

            $citiesIds = $requestParams['baden-cities'];

            $addressesIds = $this->addressesService->getAddressIdsInsideBadenRegion($citiesIds);

            if(is_array($citiesIds) && in_array(-2, $citiesIds)) {
                $addressesIds2 = $this->addressesService->getAddressIdsOutsideBaden();
            }

            $addressesIds = array_merge($addressesIds, $addressesIds2 ?? []);

            if(empty($addressesIds)) {
                $addressesIds = [-1];
            }

            $sql .= ' AND a.id IN ('.implode(',', $addressesIds).') ';
        }

        if(Auth::user()->default_country=== 'usa-private') {
            $sql .= GlobalHelper::addUsaPrivateAddressesConditions($requestParams);
        }

        if (Auth::user()->default_country !== 'usa-private' && array_key_exists('scalar-types', $requestParams)) {

            $addressIds = (new AddressesService())->selectCollectionOfAddressIdsByScalarTypes($requestParams['scalar-types']);

            $sql .= ' AND a.id IN ('.implode(',', $addressIds).')';
        }

        return $sql;
    }

    private function getAddressIdsForIteration($requestParams)
    {
        $GSS = new GlobalSearchService();
        $groupedSearchIterations = $GSS->groupSearchIterationsByEntity($requestParams['iteration']);

        $isWithLevenstein = isset($requestParams['with-levenstein']);

        $addressIds = $GSS->searchForAddressesIds($groupedSearchIterations, $isWithLevenstein, GlobalHelper::defineIsAddressesOrClustersShouldBeOrdered());

        return $addressIds;
    }

    function findCompanyByName()
    {
        $companyName = request()->get('name');

        $sql = "SELECT id, name, address FROM rl_addresses WHERE name LIKE ? LIMIT 50";

        $companies = $this->DBWithConnection()->select($sql, [$companyName.'%']);

        return response()->json($companies);
    }



    function findRelatedLabs($cluster_labs_ids = [])
    {
        $related_labs = [];

        if($cluster_labs_ids)
        {
            $sql = "SELECT * from
                (SELECT a.id, a.name, a.cluster_id, a.address FROM rl_addresses a WHERE a.id IN (" . $cluster_labs_ids . ") 
                UNION
                SELECT a2.id, a2.name, a2.cluster_id, a2.address FROM rl_addresses a  
                JOIN rl_address_people ap ON a.id = ap.address_id -- workers of main hospital
                JOIN rl_address_connections ac ON ac.from_person_id = ap.person_id -- people who know people on main hospital
                JOIN rl_address_people ap2 ON ap2.person_id = ac.to_person_id -- workplaces of people who know people on main hospital
                JOIN rl_addresses a2 ON ap2.address_id = a2.id 
                WHERE a.id IN (" . $cluster_labs_ids . ") 
                UNION 
                SELECT a2.id, a2.name, a2.cluster_id, a2.address FROM rl_addresses a  
                JOIN rl_address_people ap ON a.id = ap.address_id -- workers of main hospital
                JOIN rl_address_connections ac ON ac.to_person_id = ap.person_id -- people who know people on main hospital 
                JOIN rl_address_people ap2 ON ap2.person_id = ac.from_person_id -- workplaces of people who know people on main hospital
                JOIN rl_addresses a2 ON ap2.address_id = a2.id 
                WHERE a.id IN(" . $cluster_labs_ids . ")              
                ) related_labs ";

            $related_labs = $this->DBWithConnection()->select(DB::raw($sql));
        }
        return $related_labs;
    }




}
