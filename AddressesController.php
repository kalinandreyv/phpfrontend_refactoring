<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\AddressConnection;
use App\Models\AddressIgnore;
use App\Models\AddressPerson;
use App\Models\AddressTag;
use App\Models\AddressProduct;
use App\Models\BadenCity;
use App\Models\Cluster;
use App\Models\CustomerType;
use App\Models\Favorite;
use App\Models\People;
use App\Models\Product;
use App\Models\SwitzerlandCanton;
use App\Models\Tag;
use App\Models\Tender;
use App\Models\User;
use App\Models\UserEdit;
use App\Models\ExpanderEvents;
use App\Models\InfluencersAwards;
use App\Services\AddressesService;
use App\Services\GlobalHelper;
use App\Services\GlobalSearchService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Image;
use File;
use LabscapeRoche\LabscapeRocheService;
use stdClass;
use Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddressesController extends Controller
{

    public $addressIdsDefinedWhenIteration = [];

    public $addressesService;


    function index(AddressesService $addressesService)
    {
        $this->addressesService = $addressesService;

        $addressesForResponse = $this->fetchAddressesForUsaUser();

        return response()->json($addressesForResponse);
    }


    function loadAddressesPaginated(AddressesService $addressesService)
    {
        $addresses = $addressesService->loadAddressesPaginated();
        return response()->json($addresses);

//        $addresses = $this->composePaginatedResponseViaPureSql();
//        AddressTag::addTagsToAddresses($addresses['data']);
//        $this->addFavoriteStatusToAddresses($addresses['data']);
//        if(Auth::user()->default_country === 'usa-private') {
//            Address::addGrowthOpp($addresses['data']);
//        }
//        return response()->json($addresses);
    }

    function addFavoriteStatusToAddresses ($addresses)
    {
        foreach ($addresses as $i => $address) {
            $address->is_favorite = Favorite::isFavorite(Auth::user()->id, 'ADDRESS', $address->id);
        };
    }


    function preProcessGlobalSearch()
    {
        $searchStr = request()->all()['global-search'];

        $addressesCount = Address::prepareAddressesQuery()->count();
        $peopleCount = People::where('name', 'like', '%'.$searchStr.'%')->count();

        return response()->json([
            'count_addresses' => $addressesCount,
            'count_people' => $peopleCount
        ]);
    }

    function loadFilterValues()
    {
        if(Auth::user()->default_country === 'usa-private') {
            $rocheService = new LabscapeRocheService($this);
            $tags = $rocheService->getFilterTagValues();
            $relationalTags = [];

            $products = $rocheService->getProductFilterValues();
            $relationalProducts = $rocheService->getRelationalProductFilterValues();

            $sortByOptions = $rocheService->getSortByFilterOptions();
        }
        else {
            $tags = Tag::get(['id', 'name']);

            $tags_ = Tag::getTags();

            $relationalTags = Tag::getTagsRelational($tags_);

            $products = Product::orderByRaw('company, name')->get();

            $relationalProducts = Product::getProductsRelational($tags_);

            $sortByOptions = $this->getSortByOptions();
        }

        $personTypes = $this->DBWithConnection()->table('rl_people_types')->get();

        $customerTypes = CustomerType::visible()->get();

        $filters = [
            'tag_list' => $tags,
            'used_product_list' => $products,
            'customer_types' => $customerTypes,
            'person_types' => $personTypes,
            'relational_products' => $relationalProducts,
            'relational_tags' => $relationalTags,
            'sort_by' => $sortByOptions
        ];

        if(GlobalHelper::isWealthscapeApp()) {

            $cantons = SwitzerlandCanton::all();

            $relationalCantons = SwitzerlandCanton::getCantonsRelational();

            $filters['location_list'] = $cantons;
            $filters['relational_locations'] = $relationalCantons;


            $filters['assignee_list'] = User::getUserListForAssigning();

            $filters['baden_cities'] = $this->composeBadenCitiesFilter();
            $filters['relational_baden_cities'] = $this->composeRelationslBadenCitiesFilter($filters['baden_cities']);

        }

        return response()->json($filters);
    }


    private function composeBadenCitiesFilter()
    {
        $cities = [];

        $badenCities = BadenCity::all();

        foreach ($badenCities as $k => $city) {
            $cities[$k]['id'] = $city->id;
            $cities[$k]['name'] = $city->city;
        }

        return $cities;
    }


    private function composeRelationslBadenCitiesFilter($badenCities)
    {
        $relationalCities = [];

        $relationalCities[] = [
            'id' => -2,
            'parent_tag_id' => 0,
            'name' => 'Others',
            'childProducts' => []
        ];

        $relationalCities[] = [
            'id' => -1,
            'parent_tag_id' => 0,
            'name' => 'Bezirk Baden',
            'childProducts' => $badenCities
        ];

        return $relationalCities;
    }


    function getSortByOptions()
    {
        $options = [
            ['value' => 'name-asc', 'label' => 'Name &uarr;'],
            ['value' => 'name-desc', 'label' => 'Name &darr;'],
            ['value' => 'people-asc', 'label' => 'Employee &uarr;'],
            ['value' => 'people-desc', 'label' => 'Employee &darr;'],
        ];

        if(env('APP_SCOPE') !== 'wealthscape') {
            array_push($options, ['value' => 'products-asc', 'label' => 'Products &uarr;']);
            array_push($options, ['value' => 'products-desc', 'label' => 'Products &darr;']);
        }

        return $options;
    }

    function show($address)
    {
        $address = Address::find($address);

        if(Auth::user()->default_country === 'usa-private') {
            $address->tags = LabscapeRocheService::getAddressTagsAsArrayOfObjects($address->RSMf_SiteMasterId, $this->DBWithConnection());
        }
        else {
            $address->load('tags');
        }

        $address->load('cluster');

        $address->load(['cluster.addresses' => function($q){
            $q->take(4);
        }]);

        $address->load(['people' => function($query){
            $query->withCount('relationships');
            $query->groupBy('name');
            $query->orderBy('relationships_count', 'DESC');
            $query->take(4);
        }]);
        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);
        $address->load('tenders.purchase');

        if(!empty($address->cluster)) {
            $address->cluster->addresses_count = $address->cluster->getAddressMembersNumber();
        }

        if(Auth::user()->default_country == 'usa-private') {
            $this->addUsaPrivateSpecificFeatures($address);
        }

        $address->is_in_ignore_list = AddressIgnore::checkIfAddressIgnored($address->id);

        $address = $address->toArray();

        $address['tenders'] = Tender::where('address_id', $address['id'])->threeProductsWithMostBudgetSpent()->get();

        $address['has_relationships_for_graph'] = Address::hasRelationshipsForGraph($address['id']);

        $address['is_favorite'] = Favorite::isFavorite(Auth::user()->id, 'ADDRESS', $address['id']);

        return response()->json($address);
    }


    function addUsaPrivateSpecificFeatures(Address $address)
    {
        $address->awards = $this->getAddressAwards($address->id);

        $address->rms_ext = Address::getUsRmsExtValues($address->RSMf_SiteMasterId);

        if(!empty($address->cluster->addresses)){
            $address->cluster->addresses->each(function($item, $k) {
                $item->load(['scalar_values' => function($q){
                    $q->whereRaw('(scalar_type_id = 1 OR scalar_type_id= 10)');
                }]);
            });
        }

        $lrs = new LabscapeRocheService($this);

        $address->market_programs = $lrs->getMarketProgramsByRsmIdAndSiteLvl($address->RSMf_SiteMasterId, 'Site');
    }


    function updateCustomerStatus($address)
    {
        $address = Address::find($address);

        $data = request()->validate([
            'status' => 'required|integer',
        ]);

        $address->customer_status = $data['status'];
        $address->save();

        return response()->json($address);
    }

    function loadPeopleByAddressId ($address)
    {
        $params = request()->all();

        if(isset($params['simple-list'])) {
            return AddressPerson::getSimpelListOfEmployees($address);
        }

        $address = Address::find($address);

        if(isset($params['main-person-id']) && !AddressPerson::isPersonAnEmployee($address, $params['main-person-id'])) {
            return ['data' => [], 'total' => 0];
        }

        $people = People::with('addresses')
            ->with(['addressPerson' => function($q) use($address) {
                $q->where('address_id', $address->id);
            }])
            ->withCount('relationships')
            ->where(function ($q) use ($params) {

                if(isset($params['name'])) {
                    $q->where('name', 'like', "%$params[name]%");
                }
            })
            ->whereHas('addressPerson', function($q) use ($params){
                if(isset($params['role'])) {

                    $role = $params['role'];

                    if($role === '--empty--') {
                        $role = "";
                    }

                    $q->where('role', $role);
                }
            })
            ->whereHas('addresses', function ($q) use ($address){
                return $q->where('id', $address->id);
            })
            ->groupBy('rl_people.name')
            ->orderBy('relationships_count', 'DESC')
            ->paginate(10);

        return response()->json($people);
    }

    function getContactsChain($address)
    {

        $address = Address::find($address);

        $cluster_labs = Cluster::getClusterLabs($address->id);

        $cluster_labs_ids = implode(',', $cluster_labs);

        $related_labs = Address::$cluster_labs_ids($cluster_labs_ids);

        $related_labs_ids = "";
        $first = true;
        foreach ($related_labs as $lab){
            if ($first){
                $first = false;
            }else{
                $related_labs_ids = $related_labs_ids . ",";
            }
            $related_labs_ids = $related_labs_ids . $lab ->id;
        }

        $lab_workers = AddressPerson::findLabWorkers($related_labs_ids);

        $related_people = AddressPerson::findRelatedLabWorkers($related_labs_ids);

        // get the relations from related people
        $first = true;
        $related_people_ids = "";
        foreach ($related_people as $p){
            if ($first){
                $first = false;
            }else{
                $related_people_ids = $related_people_ids . ",";
            }
            $related_people_ids = $related_people_ids . $p->id;
        }

        $people_relationships = AddressPerson::getWorkersRelationships($related_people_ids);

        $result = [ 'related_labs' => $related_labs, 'related_people' => $related_people, 'relationships' => $people_relationships, 'workers' => $lab_workers ];

        return response()->json($result);
    }


    function getClusterMembersPaginated($address)
    {
        $address = Address::find($address);

        $search = request()->search;

        $clusterAddressesQuery = Address::where('cluster_id', $address->cluster_id);

        if(!empty($search)) {
            $clusterAddressesQuery = $clusterAddressesQuery->where('name', 'like', '%'.$search.'%');
        }

        if(Auth::user()->default_country === 'usa-private') {
            $clusterAddressesQuery->with('scalar_values');
        }

        if(Auth::user()->default_country === 'usa-private' && Auth::user()->isAclUser()) {
            $aclAddressIds = Auth::user()->getAddressIdsViaAclAddressRsmIds();

            $clusterAddressesQuery = $clusterAddressesQuery->whereIn('id', $aclAddressIds);
        }

        $clusterAddresses = $clusterAddressesQuery->paginate(10);

        return response()->json($clusterAddresses);
    }


    function getClusterStaffPaginated($address)
    {
        $request = request()->all();
        $address = Address::find($address);

        $clusterStaff = Cluster::getClusterStaff($address, $request);

        return response()->json($clusterStaff);
    }

    function getClusterProductsPaginated($address)
    {
        $address = Address::find($address);

        $products = Cluster::getClusterProducts($address);

        return response()->json($products);
    }

    /**
     * update Address
     */
    public function updateAddressDetails($address)
    {
        $address = Address::find($address);

        $params = request()->only(['name', 'address', 'url', 'phone']);
        $params['updated_by'] = 'labscape';

        $address = Address::updateAddressDetails($address, $params);

        return response()->json($address);
    }

    /**
     * get all tags
     */
    public function loadAllTags($address)
    {
        $tags = Tag::all();

        return response()->json($tags);
    }

    /**
     * get selected tags for address
     */
    public function loadSelectedTags($address)
    {
        $address = Address::find($address);

        $selectedTags = $address->load('tags')->tags;

        return response()->json($selectedTags);
    }

    /**
     * get all clusters
     */
    public function getClusters()
    {
        $clusters = Cluster::get();

        return response()->json($clusters);
    }

    /**
     * update clusters
     */
    public function updateClusters($address)
    {
        $address = Address::find($address);

        $oldClusterId = $address->cluster_id;
        $address->cluster_id = request()->get('cluster_id');

        if(Auth::user()->default_country === 'usa-private') {
            $address->updated_by = 'labscape';
        }

        $address->update();
        $address->load('cluster');
        $address->load('cluster.addresses');

        Cluster::updateClusters($oldClusterId);

        return response()->json($address);
    }

    /**
     * get all products
     */
    public function getProducts()
    {
        $products = Product::orderByRaw('company, name')->get();
        return response()->json($products);
    }

    /**
     * update used products for address
     */
    public function updateProducts($address)
    {
        $address = Address::find($address);

        $selectedProducts = request('selectedProducts');

        UserEdit::logManyToMany($address, 'rl_products', $selectedProducts, $address->products()->pluck('id')->toArray());

        $address->products()->sync($selectedProducts);

        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);

        return response()->json($address);
    }

    /**
     * create new Product
     */
    public function createProduct ($address)
    {
        $address = Address::find($address);

        $company = trim(request('company'));
        $name = trim(request('name'));
        $description = trim(request('description'));

        if ( ! $company) {
            return response()->json([
                'status' => 'error',
                'message' => "Company field should not be empty"
            ]);
        }

        if ($company && ($name == "" || $name == null)) {
            $prod = Product::whereCompany($company)->where('name', '=', "")->orWhere('name', '=', null)->first();
            if ($prod) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Product with company - '$company' already exists!"
                ]);
            }
        }

        if ($company && $name) {
            $prod = Product::whereCompany($company)->whereName($name)->first();
            if ($prod) {
                return response()->json([
                    'status' => 'error',
                    'message' => "This product already exists!"
                ]);
            }
        }

        if (strlen($company) > 255 || strlen($name) > 255) {
            return response()->json([
                'status' => 'error',
                'message' => "Max count of characters is 255!"
            ]);
        }

        $product = new Product();
        $product->company = $company;
        $product->name = $name;
        $product->description = $description;

        $image = request()->file('image');

        if ($image) {
            $extension = $image->getClientOriginalExtension();

            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
                $imageName = now()->format('Y-m-d-H-i-s') . '.' . $extension;

                $img = Image::make($image->getRealPath());

                $img->resize(100, 100, function ($constraint) {

                    $constraint->aspectRatio();

                })->save(storage_path("app/public/product-images/$imageName"));

                $product->image = "/product-images/$imageName";
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => "Only jpg/jpeg/png files are allowed!"
                ]);
            }
        }

        $product->save();

        $products = Product::orderByRaw('company, name')->get();

        $addressProduct = new AddressProduct();
        $addressProduct->address_id = $address->id;
        $addressProduct->product_id = $product->id;
        $addressProduct->save();

        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);

        return response()->json([
            'products' => $products,
            'address' => $address
        ]);
    }

    /**
     * get all people for health system
     */
    public function getAllClusterStaff ($address)
    {
        $address = Address::find($address);

        $clusterStaff = People::with('addresses')
            ->whereHas('addresses', function ($q) use ($address) {
                $q->where('cluster_id', $address->cluster_id);
            })
            ->orderByRaw('name')
            ->get();

        return response()->json($clusterStaff);
    }

    /**
     * updating lab-chain name
     */
    public function updateClusterName ($address)
    {
        $address = Address::find($address);

        $cluster = Cluster::whereId($address->cluster->id)->first();
        $cluster->name = request('clusterName');
        $cluster->save();

        return response()->json($cluster);
    }

    /**
     * create new cluster if not exist
     */
    public function createCluster ($address)
    {
        $address = Address::find($address);

        $name = trim(request('name'));

        $cluster = Cluster::where('name', $name)->first();

        if ($cluster) {
            return response()->json([
                'status' => 'error',
                'message' => 'Health system already exists'
            ]);
        } else {
            $cluster = new Cluster();

            $cluster->name = $name;

            $cluster->save();

            $address->cluster_id = $cluster->id;

            $address->save();

            $cluster->load('addresses');

            return response()->json([
                'status' => 'success',
                'cluster' => $cluster
            ]);
        }
    }

    // create new relation from person to person
    public function createPersonRelation (Request $request)
    {
        $fromPersonId = request('fromPersonId');

        $toPersonId = request('toPersonId');

        $edgeType = request('edgeType');

        $edgeComment = request('edgeComment');

        $user = JWTAuth::user();

        $addressConnection = AddressConnection::where('from_person_id', $fromPersonId)
            ->where('to_person_id', $toPersonId)
            ->first();

        if ( ! empty($addressConnection)) {
            return response()->json([
                'success' => false,
                'message' => 'This connection already exists!'
            ]);
        }

        $addressConnection = new AddressConnection();

        $addressConnection->from_person_id = $fromPersonId;

        $addressConnection->from_address_id = null;

        $addressConnection->to_person_id = $toPersonId;

        $addressConnection->to_address_id = null;

        $addressConnection->edge_weight = 1;

        $addressConnection->edge_type = $edgeType;

        $addressConnection->edge_comment = $edgeComment;

        $addressConnection->edge_source = null;

        $addressConnection->user_id = $user->id;

        $addressConnection->save();

        return response()->json([
            'success' => true,
            'message' => 'ok'
        ]);
    }

    // delete relation from person to person
    public function deletePersonRelation (Request $request)
    {
        $fromPersonId = request('fromPersonId');

        $toPersonId = request('toPersonId');

        AddressConnection::where('from_person_id', $fromPersonId)
            ->where('to_person_id', $toPersonId)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'ok'
        ]);
    }

    function fetchAddressesWithoutGroupingByState()
    {
        $sql = $this->address->prepareSqlForFetchAddressesWithoutGroupingByState();

        return $result = $this->getResultFromCache($sql);
    }


    function checkIfRequestedMin2CompaniesInCluster()
    {
        if(request()->get('at-least-2-companies')){
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();

            $subSubSql = "(SELECT a0.id FROM rl_addresses AS a0 WHERE a.id = a0.id AND a0.cluster_id IN (".implode(',', $clusterIds)."))";
        }
        else {
            $subSubSql = 'a.id';
        }
        return $subSubSql;
    }

    function addAbbreviationProp($data) {

        foreach ($data as $value) {

            $count = $value->total_addresses;

            $abbrev = $count >= 10000 ? round($count / 1000) .'k' :
                $count >= 1000 ? round($count / 100) / 10  . 'k' : $count;


            $value->total_addresses_abbrev = (string)$abbrev;
        }

        return $data;
    }

    function getAbbreviationValue($count)
    {
        $abbrev = $count >= 10000 ? round($count / 1000) .'k' :
            $count >= 1000 ? round($count / 100) / 10  . 'k' : $count;

        return $abbrev;
    }

    function fetchAddressesForUsaUser()
    {
        $foundAddressNumber = $this->address->countAddressesForUSA();

        if($foundAddressNumber > 10000 && !request()->get('state') && Auth::user()->default_country == 'usa-private') {
            $data = $this->address->findAddressesAndGroupByState();
            $result['data'] = $this->addAbbreviationProp($data);
            $result['groupedByState'] = true;
        }
        elseif($foundAddressNumber > 10000) {
            $result['data'] = $this->address->findAddressesAndGroupByLatLonTile();
            $result['groupedByState'] = true;
        }
        else {
            $result = $this->fetchAddressesWithoutGroupingByState();
        }

        return $result;
    }


    function composePaginatedResponseViaPureSql()
    {
        $total = $this->address->countAddressesForUSA();

        $sql = $this->address->prepareSqlForFetchAddressesWithoutGroupingByState(true);

        $sql .= $this->composeOrderByForAddressesWithoutGroupingByState(request()->all());

        $sql .= ' LIMIT 20' . ' OFFSET ' . $this->getOffsetForPagination();

        $result = $this->getResultFromCache($sql);

        $data = [
            'total' => $total,
            'data' => $result
        ];

        return $data;
    }

    function getOffsetForPagination() {
        return 20 * (intval(request()->page) - 1);
    }


    function composeOrderByForAddressesWithoutGroupingByState($params)
    {
        $sql = '';

        if (isset($params['sort-by'])) {

            $field = explode('-',$params['sort-by'])[0];
            $direction = explode('-',$params['sort-by'])[1];

            if($field == 'people') {
                $field .= '_count';
            }
            else if($field == 'products') {
                $field .= '_count';
            }

            $sql = " ORDER BY $field $direction";
        }
        else {
            $sql = " ORDER BY people_count DESC, a.id DESC";
        }

        if (isset($params['iteration'])) {
            $sql = ' ORDER BY FIELD(a.id, '. implode(', ', $this->addressIdsDefinedWhenIteration).')';
        }

        return $sql;
    }


    function getStaffByRoleForChart($id)
    {
        $sql = "SELECT person_id, IF( IF(ap.role <> '' || ap.role IS NOT NUll, ap.role, p.role) = '', 'Others', ap.role) as role FROM rl_people as p
                JOIN rl_address_people as ap ON ap.person_id = p.id
                WHERE ap.address_id = $id
                ";

        $result = $this->getResultFromCache($sql);

        $total = count($result);

        $rolesCount = [];

        foreach ($result as $record) {
            if(empty($rolesCount[$record->role])) {
                $rolesCount[$record->role] = 0;
            }

            ++$rolesCount[$record->role];
        }

        $groupedData = ['Others' => 0];

        foreach ($rolesCount as $role => $occur) {

            $rolesCount[$role] = [];

            $rolesCount[$role]['percentage'] = $occur / $total * 100;
            $rolesCount[$role]['occur'] = $occur;
        }

        foreach ($rolesCount as $role => $values) {

            if($role === 'Others' || $values['percentage'] < 3) {
                $groupedData['Others'] += $values['occur'];
            }
            else {
                $groupedData[$role] = $values['occur'];
            }
        }

        $graphData = [];

        foreach ($groupedData as $role => $occur) {
            $graphData[] = [$role, $occur];
        }

        array_unshift($graphData, ['Role', '%']);

        return response()->json($graphData);
    }


    function getAddressAwards($id)
    {
        $lrs = new LabscapeRocheService($this);

        return $lrs->getAddressAwards($id);
    }


    function getEmployeeRoleList($id)
    {
        $roleList = AddressPerson::getEmployeeRoleList($id);

        return response()->json($roleList);
    }


    function awardsDataForGaugeChart($id)
    {
        $address = Address::find($id);

        $graphMax = InfluencerAwards::getMaxScore();

        $awardScore = InfluencerAwards::getMaxScore($address->id);

        $graphData = ['Centrality', $awardScore];

        return response()->json([
            'data' => $graphData,
            'graphMax' => $graphMax
        ]);
    }


    function getDataForGrowthTrajectoryGauge($id)
    {
        $address = Address::find($id);

        $graphMax = ExpandersEvents::getMaxScore();

        $awardScore = ExpandersEvents::getMaxScore($address->id);

        $graphData = ['Growth', $awardScore];

        return response()->json([
            'data' => $graphData,
            'graphMax' => $graphMax
        ]);
    }

    function assignAddressToUser()
    {
        $data = request()->validate([
            'userId' => 'required|numeric',
            'entityId' => 'required|numeric',
        ]);

        $address = Address::find($data['entityId']);
        $address->assigned_to = $data['userId'] == -1 ? NULL : $data['userId'];
        $address->save();

        return response()->json($address);
    }


    function loadUsaStates()
    {
        $user = Auth::user();

        if($user->default_country !== 'usa' && $user->default_country !== 'usa-private') {
            return response()->json([]);
        }

        $sql = "SELECT id, name, abv FROM rl_usa_state_coords";

        $states = $this->getResultFromCache($sql);

        return response()->json($states);
    }

    public function share(Request $request, LabscapeRocheService $service)
    {
        if(!$service->isUserCanShareAddress(auth()->id())) {
            throw new \Exception('Too many emails sent per day');
        }
        $data = $request->validate([
            'to_name' => 'required|string',
            'address_id' => 'required|exists:rl_addresses,id',
            'to_email' => 'required|email',
            'to_subject' => 'required|string',
            'to_body' => 'required|string',
        ]);

        $service->shareAddress($data);
        return;
    }


    public function setAddressIgnore()
    {
        $data = \request()->validate([
            'reason' => 'required|numeric',
            'id' => 'required|numeric',
        ]);

        $address = Address::where('id', $data['id'])->first();
        $userId = Auth::user()->id;

        if($data['reason'] == 3) {

            $address->customer_status = 1;
            $address->save();

            return response()->json($address);
        }

        $addressIgnore = AddressIgnore::where('user_id', $userId)
            ->where('address_id', $address->id)
            ->where('ignored_type', $data['reason'])
            ->first();

        if(!$addressIgnore) {
            $addressIgnore = AddressIgnore::storeUserIgnore($userId, $address->id, $data['reason']);

            return response()->json($addressIgnore);
        }
        else {
            return response()->json(['message'=>'current ignore already exists'], 409);
        }
    }


    public function removeAddressFromIgnoreList()
    {
        $data = \request()->validate([
            'id' => 'required|numeric'
        ]);

        AddressIgnore::removeAddressFromIgnore($data['id']);

        return response()->json('ok');
    }
}




